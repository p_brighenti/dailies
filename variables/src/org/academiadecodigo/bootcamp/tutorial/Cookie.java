package org.academiadecodigo.bootcamp.tutorial;

public class Cookie {

    public static int numberProduced = 0;

    // Since static fields are not associated with any particular instance, we access them by the name of the class
    // they belong to.

    // Take a look at the constructor method below:

    public Cookie() {
        Cookie.numberProduced++;
    }

    // What we're doing here is incrementing the value of the static field numberProduced by one for each time
    // an instance of Cookie is created.
    // This means that if we create 5 instances of Cookie, the value stored in numberProduced will be 5.
    // We access static fields by the name of the class -> Cookie.numberProduced

    // This value is not associated with any instance of Cookie in particular, but with the class Cookie itself!
    // Even if we don't create any instances of Cookie, we will still be able to access the value of numberProduced
    // because it belong to the class! In that case Cookie.numberProduced would be 0.
}
