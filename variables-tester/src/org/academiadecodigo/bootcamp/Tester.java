package org.academiadecodigo.bootcamp;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;

public class Tester {

    private static String intFieldName = "variableTypeCount";
    private static String stringFieldName = "statement";

    public static void test(Class sut, String option) {

        try {
            Field field1 = sut.getDeclaredField(intFieldName);
            Field field2 = sut.getDeclaredField(stringFieldName);
            Method method = sut.getDeclaredMethod("step3", Boolean.TYPE, Double.TYPE);
            Constructor<?> constructor = sut.getConstructors()[0];

            testIntField(field1, constructor.newInstance());
            testStringField(field2);
            testMethod(method);
            testOption(option);

        } catch (NoSuchFieldException | NoSuchMethodException e) {
            throw new YouMessedUpException(e.getMessage() + " is not declared");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }


    }

    private static void testIntField(Field field, Object instance) throws IllegalAccessException {
        int modifiers = field.getModifiers();

        if (!Modifier.isPrivate(modifiers)) {
            throw new YouMessedUpException(intFieldName + " should be private");
        }

        if (Modifier.isStatic(modifiers)) {
            throw new YouMessedUpException(intFieldName + " should be a non-static field");
        }

        if (field.getType() != Integer.TYPE) {
            throw new YouMessedUpException(intFieldName + " should be of primitive type int");
        }

        field.setAccessible(true);

        if (field.getInt(instance) != 4) {
            throw new YouMessedUpException(intFieldName + " value is incorrect");
        }

        System.out.println("Step 1 -> Complete");
    }

    private static void testStringField(Field field) throws IllegalAccessException {
        int modifiers = field.getModifiers();

        if (!Modifier.isPrivate(modifiers)) {
            throw new YouMessedUpException(stringFieldName + " should be private");
        }

        if (!Modifier.isStatic(modifiers)) {
            throw new YouMessedUpException(stringFieldName + " should be a non-static field");
        }

        if (field.getType() != String.class) {
            throw new YouMessedUpException(stringFieldName + " should be of reference type String");
        }

        field.setAccessible(true);

        if (!(field.get(null)).equals("I am now a master of variable declarations. For real...")) {
            throw new YouMessedUpException(stringFieldName + " has the wrong value");
        }

        System.out.println("Step 2 -> Complete");
    }

    private static void testMethod(Method method) {
        List<Class> parameterTypes = Arrays.asList(method.getParameterTypes());

        if (!parameterTypes.contains(Boolean.TYPE)) {
            throw new YouMessedUpException(method.getName() + " should have a parameter of type boolean");
        }

        if (!parameterTypes.contains(Double.TYPE)) {
            throw new YouMessedUpException(method.getName() + " should have a parameter of type double");
        }

        System.out.println("Step 3 -> Complete");
    }

    private static void testOption(String option) {
        if(!option.equals("B")) {
            throw new YouMessedUpException("Wrong option in Step 4");
        }

        System.out.println("Step 4 -> Complete");
    }
}
