package org.academiadecodigo.bootcamp.tutorial;

public class Person {

    private String name;

    // Line 5 is an example of a declaration of a non-static field AKA instance property.
    // Unlike local variables and method parameters, since they're not contained inside a method's body,
    // we need to make a decision on how the other objects will be able to access it.
    // As we've learned, we should be as restrictive as possible, so in this case, private is the correct choice.

    // Their declaration requires 3 things:
    // <access modifier>    <type>     <name>
    //     private          String      name

    // They are also declared in the body of the class, as opposed to the body of a method.
    // We should only declare instance properties when we want to store information that is specific
    // to a given instance of a class.

    // Now go back to reading Tutorial.java

    public Person(String name) {
        this.name = name;
    }

    public void sayName() {
        System.out.println(name);
    }
}
