package org.academiadecodigo.bootcamp;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

public class Tester {

    private static Class exerciseClass;
    private static Class constructorDeclaredClass;

    public static void test(Class sut, Class aux) {

        exerciseClass = sut;
        constructorDeclaredClass = aux;

        testStep1();
        testStep2();
        testStep3();
    }

    private static void testStep1() {

        Constructor constructor;
        int testValue = 3;
        String intFieldName = "number";

        try {
            constructor = exerciseClass.getDeclaredConstructor(Integer.TYPE);
            Field intField = exerciseClass.getDeclaredField(intFieldName);
            int fieldModifiers = intField.getModifiers();
            Object instance = constructor.newInstance(testValue);

            if (!Modifier.isPrivate(fieldModifiers)) {
                throw new YouMessedUpException(intFieldName + " should be private");
            }

            if (Modifier.isStatic(fieldModifiers)) {
                throw new YouMessedUpException(intFieldName + " should be a non-static field");
            }

            if (intField.getType() != Integer.TYPE) {
                throw new YouMessedUpException(intFieldName + " should be of primitive type int");
            }

            intField.setAccessible(true);

            if (intField.getInt(instance) != testValue) {
                throw new YouMessedUpException(intField.getName() + " field is not assigned the correct value");
            }

            System.out.println("Step 1 -> Complete");

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            throw new YouMessedUpException(exerciseClass.getName() + " does not have field int number declared");
        } catch (NoSuchMethodException e) {
            throw new YouMessedUpException("Missing constructor declaration from Step 1");
        }


    }

    private static void testStep2() {
        Constructor constructor;

        testConstructorDeclared();

        try {
            constructor = exerciseClass.getDeclaredConstructor(constructorDeclaredClass);
            String instanceFieldName = "coolFieldName";
            Field instanceField = exerciseClass.getDeclaredField(instanceFieldName);
            int fieldModifiers = instanceField.getModifiers();


            if (!Modifier.isPrivate(fieldModifiers)) {
                throw new YouMessedUpException(instanceFieldName + " should be private");
            }

            if (Modifier.isStatic(fieldModifiers)) {
                throw new YouMessedUpException(instanceFieldName + " should be a non-static field");
            }

            if (instanceField.getType() != constructorDeclaredClass) {
                throw new YouMessedUpException(instanceFieldName + " should be of reference type " +
                        constructorDeclaredClass.getSimpleName());
            }

            instanceField.setAccessible(true);

            Object arg = constructorDeclaredClass.getConstructor(String.class).newInstance("");

            if(instanceField.get(constructor.newInstance(arg)) != arg){
                throw new YouMessedUpException(instanceFieldName + " is not being assigned the value received as an " +
                        "argument during the constructor method invocation of class " + exerciseClass.getSimpleName());
            }

            System.out.println("Step 2 -> Complete");

        } catch (NoSuchMethodException e) {
            throw new YouMessedUpException("Missing constructor declaration from Step 2");
        } catch (NoSuchFieldException e) {
            throw new YouMessedUpException(exerciseClass.getName() + " does not have field coolFieldName declared");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static void testStep3() {
        Constructor constructor;

        try {
            constructor = exerciseClass.getDeclaredConstructor();
            Field intField = exerciseClass.getDeclaredField("number");
            Field instanceField = exerciseClass.getDeclaredField("coolFieldName");

            intField.setAccessible(true);
            instanceField.setAccessible(true);

            Object instance = constructor.newInstance();

            if(intField.getInt(instance) != 0) {
                throw new YouMessedUpException("Field " + intField.getName() + " should not have been assigned a value");
            }

            if(instanceField.get(instance) != null) {
                throw new YouMessedUpException("Field " + instanceField.getName() + " should not have been assigned a value");
            }

            System.out.println("Step 3 -> Complete");

        } catch (NoSuchMethodException e) {
            throw new YouMessedUpException("Missing constructor declaration from Step 3");
        } catch (NoSuchFieldException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

    }

    private static void testConstructorDeclared() {

        Constructor constructor;

        try {
            constructor = constructorDeclaredClass.getConstructor(String.class);
            Field field = constructorDeclaredClass.getDeclaredField("name");

            for(Field f : constructorDeclaredClass.getFields()){
                System.out.println(f);
            }

            int modifiers = field.getModifiers();

            if (!Modifier.isPrivate(modifiers)) {
                throw new YouMessedUpException("Field name from class " + constructorDeclaredClass.getSimpleName() +
                        " should be private");
            }

            if (Modifier.isStatic(modifiers)) {
                throw new YouMessedUpException("Field name from class " + constructorDeclaredClass.getSimpleName() +
                        " should be non-static");
            }

            if(field.getType() != String.class) {
                throw new YouMessedUpException("Field name from class " + constructorDeclaredClass.getSimpleName() +
                        " should be of reference type String");
            }

            String arg = "";

            field.setAccessible(true);

            if (field.get(constructor.newInstance(arg)) != arg) {
                throw new YouMessedUpException("Field name from class " + constructorDeclaredClass.getSimpleName() +
                        " is not being assigned the value received as an argument during its constructor method invocation");
            }

        } catch (NoSuchMethodException e) {
            throw new YouMessedUpException("Missing correct constructor declaration from class " +
                    constructorDeclaredClass.getSimpleName());
        } catch (NoSuchFieldException e) {
            throw new YouMessedUpException(constructorDeclaredClass.getSimpleName() + " should have field name");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
