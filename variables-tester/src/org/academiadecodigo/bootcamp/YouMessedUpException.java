package org.academiadecodigo.bootcamp;

public class YouMessedUpException extends RuntimeException {

    public YouMessedUpException(String message) {
        super(message);
    }

}
