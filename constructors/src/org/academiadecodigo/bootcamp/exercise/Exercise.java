package org.academiadecodigo.bootcamp.exercise;

import org.academiadecodigo.bootcamp.Tester;
import org.academiadecodigo.bootcamp.tutorial.ConstructorDeclared;


public class Exercise {

    // This exercise will be quite short since there isn't much to know when it comes to using
    // constructor methods. The hard part is understanding them, and that is what we covered in the tutorial.
    // If you're still not sure you're comfortable with the idea of what a constructor method is, read it again.
    // There are a couple of very important things we must understand in order for us to be comfortable using them:

    //// What does it do? (hint: it does not create new objects)
    //// How to identify a constructor method
    //// How to declare a constructor method
    //// What I SHOULD DO inside a constructor method's body
    //// What I SHOULDN'T DO inside a constructor method's body
    //// What I CANNOT DO when declaring a constructor method
    //// How do I invoke it
    //// What is a default constructor method

    // After knowing these things in the tip of your tongue, you can proceed. Otherwise, it's time to study.

    // Now, the exercise:

    // Step 1
    // In the class Exercise, declare a constructor method that has one parameter of type int.
    // The same constructor should initialize a private non-static field of type int named number, from the class Exercise,
    // with the value received as an argument during the method's invocation.

    // Step 2
    // In the class Exercise, declare a constructor method that has one parameter of type ConstructorDeclared.
    // The same constructor should initialize a private non-static field of type ConstructorDeclared named coolFieldName
    // with the value received as an argument during the method's invocation.

    // Step 3
    // In the class Exercise, declare a constructor method that has the same signature and the same body as the
    // default constructor method for this class.

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {
        Tester.test(Exercise.class, ConstructorDeclared.class);
    }
}
