package org.academiadecodigo.bootcamp.tutorial;

public class ConstructorDeclared {

    // This class should not be tampered with

    private String name;

    public ConstructorDeclared(String name) {
        this.name = name;
    }
}
