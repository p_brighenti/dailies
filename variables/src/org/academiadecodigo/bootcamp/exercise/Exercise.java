package org.academiadecodigo.bootcamp.exercise;

import org.academiadecodigo.bootcamp.Tester;

public class Exercise {

    // Step 1
    // Declare a non-static field of the type int and assign it a value equal to the number of different
    // types of variables that you learned about in this tutorial. This field should be named variableTypeCount.

    // Step 2
    // Declare a private static field of the reference type String named statement.
    // This property should be initialized with a string containing the following:
    // I am now a master of variable declarations. For real...

    // Step 3
    // Declare a method named step3 that takes two arguments, the first being of type boolean
    // and the second being of the type double
    // the method does not need to have an implementation.

    // Step 4
    // In the method Exercise#main(), there are 3 commented snippets of code.
    // Only one of them is the correct way of declaring a local variable.
    // Choose one of them and pass the corresponding string ("A", "B" or "C") as the second argument
    // of the invocation of the method on line 33.

    ///////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {

        // private int number; -> A
        // int number; -> B
        // local int number; -> C

        Tester.test(Exercise.class, "Z");
    }
}
